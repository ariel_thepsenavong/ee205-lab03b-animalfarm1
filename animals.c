///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.c
/// @version 1.0
///
/// Helper functions that apply to animals great and small
///
/// @author @Ariel Thepsenavong <arielat@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   @30 Jan 2021
///////////////////////////////////////////////////////////////////////////////
#include <string.h>
#include <stdlib.h>

#include "animals.h"



/// Decode the enum Color into strings for printf()
//char* colorName (enum Color color) {

   // @todo Map the enum Color to a string

//   return NULL; // We should never get here
// };

char* genderName(enum Gender gender){
   char* stringGender[2] = {"MALE", "FEMALE"};
   return stringGender[gender];
}

char* breedNames(enum CatBreeds breeds){
   char* stringCatBreeds[5] = {"MAIN_COON", "MANX", "SHORTHAIR", "PERSIAN", "SPHYNX"};
   return stringCatBreeds[breeds];
}

char* colorNames1(enum Color collar1_color){
   char* stringColorNames1[6] = {"BLACK", "WHITE", "RED", "BLUE", "GREEN", "PINK"};
   return stringColorNames1[collar1_color];
}

char* colorNames2(enum Color collar2_color){
   char* stringColorNames2[6] = {"BLACK", "WHITE", "RED", "BLUE", "GREEN", "PINK"};
   return stringColorNames2[collar2_color];
}
