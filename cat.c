///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file cat.c
/// @version 1.0
///
/// Implements a simple database that manages cats
///
/// @author @Ariel Thepsenavong <arielat@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   @todo 1/31/21
///////////////////////////////////////////////////////////////////////////////

#include <string.h>
#include <stdbool.h>
#include <stdio.h>

#include "cat.h"

// @todo declare an array of struct Cat, call it catDB and it should have at least MAX_SPECIES records in it


/// Add Alice to the Cat catabase at position i.
/// 
/// @param int i Where in the catDB array that Alice the cat will go
///
/// @note This terrible style... we'd never hardcode this data, but it gets us started.
//enum Gender {MALE, FEMALE};

struct Cat{
   char        name[30];
   enum Gender gender;
   enum CatBreeds breeds;
   bool        isFixed;
   float       weight;
   enum Color  collar1_color;
   enum Color  collar2_color;
   long        license;
};

struct Cat CatDB[MAX_SPECIES];

void addAliceTheCat(int i) {
   strcpy(CatDB[i].name, "Alice");
   
   enum Gender gender = FEMALE;
   CatDB[i].gender = gender;

   enum CatBreeds breeds = MAIN_COON;
   CatDB[i].breeds = breeds;

   CatDB[i].isFixed = Yes;
   CatDB[i].weight = 12.34;
   CatDB[i].license = 12345;

   enum Color collar1_color = BLACK;
   CatDB[i].collar1_color = collar1_color;
   enum Color collar2_color = RED;
   CatDB[i].collar2_color = collar2_color;
}



/// Print a cat from the database
/// 
/// @param int i Which cat in the database that should be printed
///
void printCat(int i) {
   // Here's a clue of what one printf() might look like...
   // printf ("    collar color 1 = [%s]\n", colorName( catDB[i].collar1_color ));
   printf("Cat name = [%s]\n", CatDB[i].name);
   printf("gender = [%s]\n", genderName(CatDB[i].gender));
   printf("breed = [%s]\n", breedNames(CatDB[i].breeds)); 
   printf("isFixed = [%s]\n", CatDB[i].isFixed ? "Yes" : "No");
   printf("weight = [%f]\n", CatDB[i].weight);
   printf("collar color 1 = [%s]\n", colorNames1(CatDB[i].collar1_color)); 
   printf("collar color 2 = [%s]\n", colorNames2(CatDB[i].collar2_color)); 
   printf("license = [%d]\n", CatDB[i].license);
}

