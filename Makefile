###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 03b - Animal Farm 1
#
# @file Makefile
# @version 1.0
#
# @author @todo Ariel Thepsenavong <@todo arielat@hawaii.edu>
# @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
# @date   @todo 30 Jan 2021
###############################################################################

all: animalfarm

animals.o: animals.c animals.h
	gcc -c animals.c

cat.o: cat.c cat.h animals.h
	gcc -c cat.c

main.o: main.c cat.h
	gcc -c main.c

animalfarm: animals.o cat.o main.o
	$(info You need to write your own Makefile)
	$(info I know you can do it)
	$(info for now type gcc -o animalfarm *.c)
	gcc -o animalfarm animals.o cat.o main.o
clean:
	rm -f *.o animalfarm
